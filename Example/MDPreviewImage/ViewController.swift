//
//  ViewController.swift
//  MDPreviewImage
//
//  Created by 85940969@qq.com on 01/06/2018.
//  Copyright (c) 2018 85940969@qq.com. All rights reserved.
//

import UIKit
import MDPreviewImage

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "MDPreviewImage"
        
        // Do any additional setup  after loading the view, typically from a nib.
    }
    @IBAction func showImage(_ sender: Any) {
        let imageUrls = ["https://upload-images.jianshu.io/upload_images/3169555-d92152a537124799.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/510","https://upload-images.jianshu.io/upload_images/3398242-95b0937780e150a3.JPG?imageMogr2/auto-orient/strip%7CimageView2/2/w/700","https://upload-images.jianshu.io/upload_images/3398242-101aadebaac7234e.JPG?imageMogr2/auto-orient/strip%7CimageView2/2/w/700","https://upload-images.jianshu.io/upload_images/3398242-b4fd492cfd2c6a7a.JPG?imageMogr2/auto-orient/strip%7CimageView2/2/w/700","https://upload-images.jianshu.io/upload_images/3398242-88f3bde731d05420.JPG?imageMogr2/auto-orient/strip%7CimageView2/2/w/700"]
        
        var images:[MDPreviewImageModel] = []
        for url in imageUrls{
            let model = MDPreviewImageModel()
            model.type = .url
            model.url = url
            images.append(model)
        }
        let vc =  MDPreviewImageViewController.init(images: images, index: 0)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }

}

