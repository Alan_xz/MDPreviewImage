# MDPreviewImage

[![CI Status](http://img.shields.io/travis/85940969@qq.com/MDPreviewImage.svg?style=flat)](https://travis-ci.org/85940969@qq.com/MDPreviewImage)
[![Version](https://img.shields.io/cocoapods/v/MDPreviewImage.svg?style=flat)](http://cocoapods.org/pods/MDPreviewImage)
[![License](https://img.shields.io/cocoapods/l/MDPreviewImage.svg?style=flat)](http://cocoapods.org/pods/MDPreviewImage)
[![Platform](https://img.shields.io/cocoapods/p/MDPreviewImage.svg?style=flat)](http://cocoapods.org/pods/MDPreviewImage)

![example1](preview.gif)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

iOS 8.0+ Swift 4

## Installation

MDPreviewImage is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MDPreviewImage'
```

## Author

Alan_xz, 85940969@qq.com

## License

MDPreviewImage is available under the MIT license. See the LICENSE file for more info.
