//
//  MDPreviewImageModel.swift
//  MDSolveWorries
//
//  Created by Alan on 2018/1/6.
//  Copyright © 2018年 MD. All rights reserved.
//

import UIKit

public enum MDImageType : Int {
    case defaut //为本地图片
    case url   //为网络图片
}


public class MDPreviewImageModel: NSObject {
    public var type:MDImageType = .defaut
    public var image:UIImage?
    public var url:String?
}
